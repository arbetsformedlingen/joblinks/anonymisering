import sys
import jsonlines
from loguru import logger
from anonymization import anonymizer
import nltk
nltk.download("punkt")

if __name__ == '__main__':
    """
    reads a single jsonlines input file ad by ad, anonymizes the ad and writes it immediately to the output file
    
    python main.py input_file.jsonl 
    
    """
    logger.info("start")
    anonymize = anonymizer.FastAnonymizer()
    counter = 0
    file_name = sys.argv[1]
    output_file_name = f"{file_name}-ANON.jsonl"
    logger.info(f"Anonymizing {file_name} to {output_file_name} ")
    with jsonlines.open(file_name) as in_file, jsonlines.open(output_file_name, "w") as out_file:
        for ad in in_file:
            result = anonymize.process_ad(ad)
            out_file.write(result)
            counter += 1
    logger.info(f"Anonymized {counter} ads to {output_file_name}")
