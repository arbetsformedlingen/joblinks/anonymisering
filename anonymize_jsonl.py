import jsonlines
import datetime
import time
import nltk
from loguru import logger
from anonymization import anonymizer

in_file_name = r"C:\repos\Create-historical-from-stream\2022_summarized.jsonl"
out_file_name = '2022_anonymized_2023_03_09.jsonl'

if __name__ == '__main__':

    begin_time = datetime.datetime.now()
    logger.info("Starting anonymization")
    nltk.download("punkt")
    anonymize = anonymizer.FastAnonymizer()

    logger.info("init ready")
    ads = []

    counter = 0

    timings = []

    with jsonlines.open(in_file_name) as input_file, jsonlines.open(out_file_name, "w") as output_file:
        logger.info(f"opened input file: {in_file_name}")
        for ad in input_file:
            start = time.time()
            anon_ad = anonymize.process_ad(ad)

            timings.append(time.time() - start)
            output_file.write(anon_ad)
            counter += 1

    sum = 0
    for item in timings:
        sum += item
    logger.info(f"Average: {sum / len(timings)}")

    execution_time = datetime.datetime.now() - begin_time
    logger.info(f"Execution time: {execution_time}")

    logger.info(f"{counter} ads written to {out_file_name}")
    logger.info(f"Execution time: {execution_time}")
    logger.info("completed")
