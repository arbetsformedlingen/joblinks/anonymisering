# Anonymisering

Anonymization of Swedish text in job ads in JobSearch format. Common use cases for this is when creating
historical ads files, see guide for generating historical ads files and historical joblinks files:
https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers/-/blob/main/docs/create_historical_file.md?ref_type=heads
And
https://gitlab.com/arbetsformedlingen/joblinks/remove-union-representatives-from-description/-/blob/master/README.md?ref_type=heads

- Removes common names (from SCB statistics)
- Masks email addresses
- Masks telephone number-looking strings written without spaces ( e.g. 08123456 )
- Remove entire sentence where union representative(s) are mentioned.
- Remove entire sentence where telephone number-looking strings are found (e.g. 08-12 34 56).

## Setup

Create a virtual environment and run `pip install -r requirements.txt`
Additionally, an NLTK package for Swedish text will be downloaded when the program runs, so an internet connection is
required at runtime.
Might need to install nltk manually:
In a terminal using this virtual environment:
`py
import nltk
nltk.download('punkt')`

## Tests

`pytest tests` runs all the unit tests

## Anonymize a jsonlines file

Only jsonlines files are supported
`python main.py <input-file-name>.jsonl`
Output will be a file with the same path and name, but with the suffix `-ANON.jsonl`
