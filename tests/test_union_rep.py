import pytest
from tests.resources.helper import compare_strings


def test_union_rep_english(anon):
    input_text = "Some text before. The union representative Sven Svensson can be reach on email union@example.com or by phone 012345667. More text after."
    expected = "Some text before. More text after."
    result = anon.remove_union_representatives(input_text)
    compare_strings(result, expected)


@pytest.mark.parametrize("input_text, expected", [
    ('En första mening utan namn. Kontakta vår fackliga företrädare Bosse Bengtsson. En sista mening utan namn.',
     'En första mening utan namn. En sista mening utan namn.'),
    (
            'Facklig representant är Anna Persson, 070-123123. Om du har lust och möjlighet så uppmuntrar vi dig gärna att spela in ett videoklipp där du kortfattat berättar mer om dig själv och varför du är intresserad av att arbeta hos oss. Videoklippet kan du sedan bifoga i vårt HR-system. Din presentation via video blir ett komplement till din ansökan och hjälper oss att skapa en bild av vem du är.',
            'Om du har lust och möjlighet så uppmuntrar vi dig gärna att spela in ett videoklipp där du kortfattat berättar mer om dig själv och varför du är intresserad av att arbeta hos oss. Videoklippet kan du sedan bifoga i vårt HR-system. Din presentation via video blir ett komplement till din ansökan och hjälper oss att skapa en bild av vem du är.'),
    (
            'En första mening utan telefon. Kontakta vår fackliga företrädare på telefon 08-123123123. En sista mening utan telefon.',
            'En första mening utan telefon. En sista mening utan telefon.'),
    (
            'En första mening utan email. Kontakta vår fackliga företrädare genom att skriva till adam@example.com. En sista mening utan email.',
            'En första mening utan email. En sista mening utan email.'),
    ("""En första mening utan personuppgifter men med radbrytningar.
            Fackliga företrädare:
            Bosse Bengtsson tel: 08 123 4567.
            En sista mening utan personuppgifter. Behåll radbrytningar""",
     """En första mening utan personuppgifter men med radbrytningar. En sista mening utan personuppgifter. Behåll radbrytningar"""),

    ("""En första mening utan namn. Kontakta vår fackliga företrädare Bosse Bengtsson. En sista mening utan namn."""
     , 'En första mening utan namn. En sista mening utan namn.'
     )
])
def test_union_representative(input_text, expected, anon):
    # TODO: fix line breaks
    """
    Whole sentences are removed
    """
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)


@pytest.mark.parametrize("input_text, expected", [
    ("""1 En första mening utan namn. Hos oss arbetar Bosse Bengtsson.
En sista mening utan namn.""", """1 En första mening utan namn. Hos oss arbetar Bosse Bengtsson.
En sista mening utan namn."""),

    ("""2 En första mening utan personuppgifter. En andra mening utan personuppgifter.
En sista mening utan personuppgifter.""", """2 En första mening utan personuppgifter. En andra mening utan personuppgifter.
En sista mening utan personuppgifter."""),
    ("""3 En första mening utan personuppgifter. För Fackliga kontaktpersoner se https://www.justatest.com/sv/karriar/kontakta-oss/facklig-kontakt/
    En sista mening utan personuppgifter.""", """3 En första mening utan personuppgifter."""),

    ("""4 En första mening utan telefon. Ring oss på telefon 08-123123123.
En sista mening utan telefon.""",
     """4 En första mening utan telefon. Ring oss på telefon 08-123123123. En sista mening utan telefon."""),
    ("""5 En första mening utan email. Skriv till adam@example.com En sista mening utan email.""",
     """5 En första mening utan email. Skriv till adam@example.com En sista mening utan email."""),
    (
            """6 En första mening utan personuppgifter. Vi är stolta över vår fackliga företrädare och denna mening ska inte tas bort. En sista mening utan personuppgifter.""",
            """6 En första mening utan personuppgifter. En sista mening utan personuppgifter."""),
    ("""7 Fackliga kontaktpersoner med radbrytningar 
Information om fackliga kontaktpersoner, se Hjälp för sökande.""", """"""),
    ("""8 En första mening utan namn. Hos oss arbetar Bosse Bengtsson.
En sista mening utan namn.""", """8 En första mening utan namn. Hos oss arbetar Bosse Bengtsson.
En sista mening utan namn."""),
    ("""9 En första mening utan personuppgifter. En andra mening utan personuppgifter.
En sista mening utan personuppgifter.""", """9 En första mening utan personuppgifter. En andra mening utan personuppgifter.
En sista mening utan personuppgifter."""),
    ("""10 En första mening utan personuppgifter. En andra mening utan personuppgifter.
En sista mening utan personuppgifter.""",
     """10 En första mening utan personuppgifter. En andra mening utan personuppgifter.
En sista mening utan personuppgifter."""),
    ("""11 En första mening utan telefon. Ring oss på telefon 08-123123123.
En sista mening utan telefon.""",
     """11 En första mening utan telefon. Ring oss på telefon 08-123123123. En sista mening utan telefon."""),
    ("""12 En första mening utan email. Skriv till adam@example.com En sista mening utan email.""",
     """12 En första mening utan email. Skriv till adam@example.com En sista mening utan email."""),
    (
            """14 En första mening utan personuppgifter. Vi är stolta över vår fackliga företrädare och denna mening ska tas bort. En sista mening utan personuppgifter.""",
            """14 En första mening utan personuppgifter. En sista mening utan personuppgifter."""),

    ("""15. Fackliga kontaktpersoner med radbrytningar 
Information om fackliga kontaktpersoner, se Hjälp för sökande.""", "15."),

])
def test_anonymize_unionrep_name_line_breaks(input_text, expected, anon):
    """
    Names can be included in the response
    :param input_text:
    :param expected:
    :param anon:
    :return:
    """
    result = anon.remove_union_representatives(input_text)
    compare_strings(result, expected)
