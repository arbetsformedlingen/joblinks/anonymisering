import pytest

from tests.resources.helper import compare_strings


def test_split_sentences_punkt(anon):
    input_text = """En första mening med punkt och mailadress info@example.com. En andra mening med punkt och mailadress info@example.com."""
    expected = "En första mening med punkt och mailadress **** En andra mening med punkt och mailadress ****"
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)


def test_split_sentences_newline(anon):
    input_text = """En första mening utan punkt med radbrytning på slutet och mailadress info@example.com
                    En andra mening utan punkt med radbrytning på slutet och mailadress info@example.com
                    En tredje mening utan punkt och utan radbrytning på slutet och mailadress info@example.com"""
    expected = 'En första mening utan punkt med radbrytning på slutet och mailadress ' \
               '****                    En andra mening utan punkt med radbrytning på slutet ' \
               'och mailadress ****                    En tredje mening utan punkt och utan ' \
               'radbrytning på slutet och mailadress ****'
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)


def test_split_sentences_exclamation_mark(anon):
    """
    The whole word with an @ character is replaced, including trailing separators

    """
    input_text = """En första mening med utropstecken på slutet och mailadress info@example.com! En andra mening utan punkt och mailadress info@example.com"""
    expected = "En första mening med utropstecken på slutet och mailadress **** En andra mening utan punkt och mailadress ****"
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)


def test_anonymize_name_clashing_with_common_word(anon):
    # Note: Make sure that the word 'Dina' is not considered a person name
    # (clashing with a common swedish or english term).
    input_text = """En första mening utan namn. Dina kompetenser är viktiga för oss. En sista mening utan namn men med dina ."""
    expected = "En första mening utan namn. Dina kompetenser är viktiga för oss. En sista mening utan namn men med dina ."
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)


def test_anonymize_name(anon):
    input_text = """En första mening utan namn. En andra mening med namnet Bosse Bengtsson. En sista mening utan namn."""
    expected = "En första mening utan namn. En sista mening utan namn."
    result = anon._anonymize_ad_description(input_text)
    assert result == expected
    compare_strings(result, expected)


def test_anonymize_dash(anon):
    input_text = """En första mening utan namn. En andra mening med namnet\n -Bosse Bengtsson. En sista mening utan namn."""
    expected = "En första mening utan namn. En sista mening utan namn."
    result = anon._anonymize_ad_description(input_text)
    assert result == expected
    compare_strings(result, expected)


def test_anonymize_phonenumber(anon):
    input_text = """En första mening utan telefonnummer. En andra mening med telefonnummer 0702-123456. En sista mening utan telefonnummer."""
    expected = "En första mening utan telefonnummer. En andra mening med telefonnummer **** En sista mening utan telefonnummer."
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)


def test_anonymize_email(anon):
    input_text = """En första mening utan epost.
En andra mening med epost bosse1.bengtsson@test.se.
En tredje mening med epost bosse2@test.se.
En sista mening utan epost."""
    result = anon._anonymize_ad_description(input_text)
    expected = """En första mening utan epost.
En andra mening med epost **** En tredje mening med epost **** En sista mening utan epost."""
    compare_strings(result, expected)


def test_anonymize_all_types(anon):
    input_text = """Start rad 1. Lars Magnus Ericsson var son till hemmansägaren Erik Ericsson (1804-1858) från Vegerbol i Värmskog. Slut rad 1.
Start rad 2. Hej! Mitt namn är Adam Svensson. Min epost-adress är info@example.com. Du kan nå mig på telefon 0702-123456. Slut rad 2.
Start rad 3. Var god kontakta Abbe på lagret. Eller Ibrahim på ibra@example.com. Slut rad 3."""

    result = anon._anonymize_ad_description(input_text)
    expected = """Start rad 1. Slut rad 1. Start rad 2. Hej! Min epost-adress är **** Du kan nå mig på telefon **** Slut rad 2. Start rad 3. Slut rad 3."""
    compare_strings(result, expected)


def test_anonymize_with_extra_keywords(anon):
    input_text = """En första mening utan känslig data.
                    En andra mening med känslig data, facklig företrädare hos oss är XYZ.
                    En tredje mening med känslig data, fackliga företrädare hos oss är XYZ och ZYX.
                    En sista mening utan känslig data."""

    result = anon._anonymize_ad_description(input_text)
    expected = "En första mening utan känslig data. En sista mening utan känslig data."
    compare_strings(result, expected)


def test_one_sentence_remove_all(anon):
    """
    This is one sentence, so everything will be removed by the union rep rule
    """
    input_text = """
Upplysningar om tjänsten lämnas av:
Centrumchef, Karin Testsson, 010-123 12 34, karin@testsson.se
Lokala fackliga företrädare:
Läkarförbundet är överläkare reumatologiska kliniken, Bosse Bengtsson, bosse@bengtsson.se, 
Fackliga företrädare för:
Vårdförbundet är Kalle Karlsson, kalle@karlsson.se
Vision är Maja majasson, maja@majasson.se
Kommunal är Frida Frid, frida@frid.se
Ansökan
Sista ansökningsdag är 2 november 2020.
"""
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, "")  # this is one sentence, so it will be deleted by the union rep rule


def test_with_sentences(anon):
    input_text = """
Upplysningar om tjänsten lämnas av:
Centrumchef, Karin Testsson, 010-123 12 34, karin@testsson.se.
Lokala fackliga företrädare:
Läkarförbundet är överläkare reumatologiska kliniken, Bosse Bengtsson, bosse@bengtsson.se. 
Fackliga företrädare för:
Vårdförbundet är Kalle Karlsson, kalle@karlsson.se.
Vision är Maja majasson, maja@majasson.se.
Kommunal är Frida Frid, frida@frid.se.
Ansökan.
Sista ansökningsdag är 2 november 2020.
"""
    result = anon._anonymize_ad_description(input_text)
    expected = "Ansökan. Sista ansökningsdag är 2 november 2020."
    compare_strings(result, expected)


@pytest.mark.parametrize("input_text, expected", [
    ("annons Oskar mellanslag", ""),
    ("annons Oskar. punkt", "punkt"),
    ("annons Oskar, komma", ""),

])
def test_anon_text(input_text, expected, anon):
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)


@pytest.mark.parametrize("input_text, expected", [
    ("annons Oskar@example.com mellanslag", "annons **** mellanslag"),
    ("annons Oskar@example.com. punkt", "annons **** punkt"),
    ("annons Oskar@example.com, komma", "annons **** komma"),
])
def test_anon_email(input_text, expected, anon):
    result = anon._remove_email(input_text)
    compare_strings(result, expected)


def test_email_and_dot(anon):
    input_text = """En första mening med punkt och mailadress adam@example.com. En andra mening med punkt och mailadress adam@example.com."""
    expected = 'En första mening med punkt och mailadress **** En andra mening med punkt och mailadress ****'
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)


def test_with_line_breaks(anon):
    input_text = """En första mening utan punkt med radbrytning på slutet och mailadress adam@example.com
                    En andra mening utan punkt med radbrytning på slutet och mailadress adam@example.com
                    En tredje mening utan punkt och utan radbrytning på slutet och mailadress adam@example.com"""
    expected = ('En första mening utan punkt med radbrytning på slutet och mailadress '
                '****                    En andra mening utan punkt med radbrytning på slutet '
                'och mailadress ****                    En tredje mening utan punkt och utan '
                'radbrytning på slutet och mailadress ****')
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)


def test_whole_ad(base_ad, anon):
    base_ad["description"]["text"] = """annonsbeskrivning. Kontaktperson är Jan Ersa. Epost jan.ersa@example.com. Telefon 012-345678. 
    Alternativt 070 - 12 34 56 .  Facklig företrädare är Per Persa, per.persa@example.com 098-7654321. Avslutningstext. 
    """
    result = anon.process_ad(base_ad)
    expected = {'employer': {'phone_number': None, 'email': None, 'organization_number': '2021002114'},
                'application_details': {'information': None, 'reference': None, 'email': None, 'url': None,
                                        'other': None},
                'description': {'text': 'annonsbeskrivning. Epost **** Avslutningstext.', 'text_formatted': None},
                'application_contacts': None, 'webpage_url': None, 'keep_this': 'important text do not remove',
                'employment_type': {}, 'salary_type': {}, 'duration': {}, 'working_hours_type': {}, 'scope_of_work': {},
                'occupation': {}, 'occupation_group': {}, 'occupation_field': {}, 'workplace_address': {},
                'must_have': {}, 'nice_to_have': {}}
    assert result == expected


def test_text_with_multiple_simple_phone_numbers(anon):
    input_text = """lite text före telefonnumret 700123456 och mer text innan nästa nummer 0700123456 och avslutningstext. Sen en mening till med nummer ett +46700123456, nummer två 0046700123456"""
    expected = """lite text före telefonnumret **** och mer text innan nästa nummer **** och avslutningstext. Sen en mening till med nummer ett **** nummer två ****"""
    result = anon._anonymize_ad_description(input_text)
    compare_strings(result, expected)
