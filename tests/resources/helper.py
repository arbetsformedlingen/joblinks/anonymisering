def compare_strings(actual: str, expected: str):
    # remove line breaks before comparison
    assert actual.replace("\n", " ") == expected.replace("\n", " ")
