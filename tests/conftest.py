import random
from pathlib import Path

import pytest

import anonymization.anonymizer


@pytest.fixture(scope="session")
def anon():
    """
    Initialize anonymization class once for all tests
    """
    return anonymization.anonymizer.FastAnonymizer()


@pytest.fixture()
def load_test_personal_numbers() -> list:
    return _read_file("Testpersonnummer_170117.csv")


@pytest.fixture()
def load_test_coordination_numbers() -> list:
    # samordningsnummer for those who do not have a personal number
    return _read_file("Testsamordningsnummer_2022.csv")


@pytest.fixture()
def create_org_numbers() -> list:
    org_numbers = []
    # prefixes for different company types
    for prefix in ['21', '22', '31', '32', '41', '42', '43', '49', '51', '53', '54', '55', '61', '62', '63', '71', '72',
                   '81', '82', '83', '84', '85', '87', '88', '89', '91', '92', '93', '94', '95', '96', '98', '99']:
        # the "month" part of an organization number is always 20 or more
        # i.e. if there is an actual month (01-12) it's a personal number
        mm = random.randint(20, 99)
        # fill upp with digits to a lenght of 10
        last_part = random.randint(100000, 999999)
        org_nr = f"{prefix}{mm}{last_part}"
        org_numbers.append(org_nr)
    return org_numbers


@pytest.fixture()
def base_ad() -> dict:
    return {
        "employer": {
            "phone_number": 1234567890,
            "email": "noreply@example.com",
            "organization_number": "2021002114"

        },
        "application_details": {
            "information": "hello",
            "reference": "my reference",
            "email": " apply@example.com",
            "url": "http://something"
        },
        "description": {
            "text": "hello world",
            "text_formatted": "hello formatted text"
        },
        "application_contacts": {
            "name": "abc",
            "email": "contact@example.com"
        },
        "webpage_url": "https://something",
        "keep_this": "important text do not remove",
        "employment_type": [],
        "salary_type": {},
        "duration": {},
        "working_hours_type": {},
        "scope_of_work": {},
        "occupation": {},
        "occupation_group": {},
        "occupation_field": {},
        "workplace_address": {},
        "must_have": {},
        "nice_to_have": {},
    }


def _read_file(filename):
    numbers = []
    resources_dir = Path(__file__).resolve().parent / "resources"
    filename = resources_dir / filename
    with open(filename) as f:
        data = f.readlines()
    for row in data:
        numbers.append(row.rstrip())
    return numbers
