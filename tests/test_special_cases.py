def test_special_do_not_mask(anon):
    input_text = "Dina arbetsuppgifter"
    result = anon._anonymize_ad_description(input_text)
    assert result == input_text


def test_special_mask(anon):
    input_text = "Dina Svensson"
    result = anon._anonymize_ad_description(input_text)
    assert result == ""


def test_special_mask_multiple_occurences(anon):
    input_text = "Dina arbetsuppgifter bestäms av Dina Svensson och"
    result = anon._anonymize_ad_description(input_text)
    expected = ""
    assert result == expected


def test_special_mask_multiple_sentences(anon):
    input_text = "Dina arbetsuppgifter är. Din chef är Dina Svensson och"
    result = anon._anonymize_ad_description(input_text)
    expected = "Dina arbetsuppgifter är."
    assert result == expected


def test_words_that_maybe_should_be_included(anon):
    """
    Some of these could be last names and require a check if the previous word is masked
    :return:
    """
    input_text = "Lund Berlin Malta Burger King Power Fenix Key, Nanny, Avesta Linux  Lexman, Life Field The Europa London Karlstad, Kalmar Junior Apple English Ofelia Astra Paris France Tumba Kiruna Lund Jira China Albano "
    expected = ""
    result = anon._anonymize_ad_description(input_text)
    assert result == expected
