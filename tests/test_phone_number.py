import pytest
from anonymization import telephone_number_finder


@pytest.mark.parametrize("phone_number", [
    "700123456",
    "0700123456",
    "+46700123456",
    "0046700123456",
])
def test_mask_phone_number(phone_number):
    number_with_padding = f"Mening före. Kontakta mig på {phone_number} eller. Mening efter."

    result = telephone_number_finder.handle_telephone_numbers(number_with_padding)
    assert result == "Mening före. Kontakta mig på **** eller. Mening efter."


@pytest.mark.parametrize("phone_number", ["018-123 45 67",
                                          "010 – 123 00 00",
                                          "031-10 00 000.",
                                          "0911-10 00 00",
                                          "070 - 12 34 56",
                                          "070 - 12 34 56.",
                                          ])
def test_remove_sentence(phone_number):
    """
    Telehone numbers written with spaces can't be masked because the search is done
    in text without spaces, and we can recreate removed spaces. In this case, the whole sentence with the number is removed
    :param phone_number:
    :return:
    """
    number_with_padding = f"Mening före. Kontakta mig på {phone_number} eller. Mening efter."
    result = telephone_number_finder.handle_telephone_numbers(number_with_padding)
    assert result == "Mening före. Mening efter."
