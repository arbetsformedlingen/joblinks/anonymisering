from anonymization import field_remover
af_org_nr = "2021002114"


def test_field_removal(base_ad):
    cleaned_ad = field_remover.remove_ad_fields(base_ad)
    expected = {'employer': {'phone_number': None, 'email': None, 'organization_number': '2021002114'}, 'application_details': {'information': None, 'reference': None, 'email': None, 'url': None, 'other': None}, 'description': {'text': 'hello world', 'text_formatted': None}, 'application_contacts': None, 'webpage_url': None, 'keep_this': 'important text do not remove', 'employment_type': {}, 'salary_type': {}, 'duration': {}, 'working_hours_type': {}, 'scope_of_work': {}, 'occupation': {}, 'occupation_group': {}, 'occupation_field': {}, 'workplace_address': {}, 'must_have': {}, 'nice_to_have': {}}

    assert cleaned_ad == expected


def test_remove_personal_number(base_ad):
    test_personal_number = "201701012393"
    employer_value = base_ad["employer"]
    employer_value.update({"organization_number": test_personal_number})  # personal number

    base_ad["employer"] = employer_value
    cleaned_ad = field_remover.remove_field_if_not_organization_number(base_ad)
    assert cleaned_ad["employer"]["organization_number"] is None


def test_do_not_remove_org_nr(base_ad):
    cleaned_ad = field_remover.remove_field_if_not_organization_number(base_ad)
    assert cleaned_ad["employer"]["organization_number"] == af_org_nr
