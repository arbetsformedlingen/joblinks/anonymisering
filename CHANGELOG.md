Changelog
===============================
# January 2024
* Fix files paths in tests
* Update README
* Remove unnecessary files
* Update dependencies

# November 2022
* Code cleanup
* Use sets instead of lists for better performance

# October 2022
* Remove "webpage_url"
