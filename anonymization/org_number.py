def is_organization_number(number_to_check: str) -> bool:
    # oragnization number could have a dash that needs to be removed for further checks
    if all([len(number_to_check) == 11, "-" in number_to_check]):
        number_to_check = number_to_check.replace("-", "")

    if len(number_to_check) != 10:
        # if length is not correct for an organization number, no point in more checks
        return False
    if number_to_check.startswith("0"):
        # not an org number
        return False
    # if it's a 10-digit number,
    # the MM part is 20 or more in organization numbers (i,e not a month)
    org_nr_part = number_to_check[2:4]
    if all([int(org_nr_part) >= 20, org_nr_part.isdigit(), ]):
        return True
    return False
