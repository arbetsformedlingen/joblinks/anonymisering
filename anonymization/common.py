import re
import nltk

mask = "****"

regex_phone_number_complex = re.compile(r'^([+]46)\s*(7[0236])\s*(\d{4})\s*(\d{3})$')
regex_phone_number_simple = re.compile(r'[0-9]{6,7}')

tokenizer = nltk.data.load('nltk:tokenizers/punkt/swedish.pickle')


def text_to_sentences(text: str) -> list:
    sentences = tokenizer.tokenize(text)
    return sentences


def list_to_str(list_of_words: list) -> str:
    return " ".join(list_of_words)
